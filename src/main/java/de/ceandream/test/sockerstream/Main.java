package de.ceandream.test.sockerstream;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static List<Connection> connectionList;
    public static Server server;

    public static void main(String[] args) {
        connectionList = new ArrayList<>();

        server = new Server();
        server.start();
    }

    public static List<Connection> getConnectionList() {
        return connectionList;
    }

    public static Server getServer() {
        return server;
    }
}
