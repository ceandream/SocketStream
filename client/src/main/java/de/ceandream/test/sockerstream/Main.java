package de.ceandream.test.sockerstream;


import java.io.IOException;
import java.util.Scanner;

@SuppressWarnings("deprecation")
public class Main {

    public static Client client;

    public static void main(String[] args) {
        Scanner scn = new Scanner(System.in);

        System.out.println("Gebe bitte den Host an:");
        String host = scn.next();
        System.out.println("Gebe bitte den Port an:");
        int port = scn.nextInt();

        client = new Client(host,port);
        client.start();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                client.getSocket().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("Schließe verbindung.");
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            client.stop();

            System.out.println("Client wurde gestoppt!");
            System.out.println("Tschuess");
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }));
    }


}
