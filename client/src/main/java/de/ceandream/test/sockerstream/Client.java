package de.ceandream.test.sockerstream;

import java.io.*;
import java.net.Socket;
import java.util.Arrays;

@SuppressWarnings({"deprecation", "ConstantConditions"})
public class Client extends Thread {

    private String host;
    private int port;
    private BufferedInputStream inputStream;
    private BufferedOutputStream outputStream;
    private Socket socket;

    public Client(String host, int port) {
        this.host = host;
        this.port = port;
    }

    @Override
    public void run() {

        try {
            socket = new Socket(host,port);

            inputStream = new BufferedInputStream(socket.getInputStream());
            outputStream = new BufferedOutputStream(socket.getOutputStream());

            System.out.println("Verbunden mit " + host + ":" + port);

            ByteArrayOutputStream b = new ByteArrayOutputStream();
            DataOutputStream out = new DataOutputStream(b);

            try {
                out.writeUTF("Hey!");
                out.writeUTF("Ich bin ein Client!");

                write(b.toByteArray());

                out.close();
                b.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            while (socket.isConnected()) {

                String line;

                DataInputStream in = new DataInputStream(inputStream);

                while ((line = in.readUTF()) != null) {
                    System.out.println(line);
                }


            }

            System.out.println("Verbindung mit " + host + ":" + port + " wurde unterbrochen.");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public BufferedInputStream getInputStream() {
        return inputStream;
    }

    public BufferedOutputStream getOutputStream() {
        return outputStream;
    }

    public void write (byte[] bytes) {
        try {
            outputStream.write(bytes);
            outputStream.flush();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public Socket getSocket() {
        return socket;
    }
}
