package de.ceandream.test.sockerstream;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server extends Thread {

    @Override
    public void run() {


        try {
            ServerSocket serverSocket = new ServerSocket(2525);

            while (!(serverSocket.isClosed())) {
                Socket accept = serverSocket.accept();

                Connection connection = new Connection(accept);
                connection.start();

                Main.getConnectionList().add(connection);

                System.out.println(accept.getInetAddress().getHostAddress() + " Connected!");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
