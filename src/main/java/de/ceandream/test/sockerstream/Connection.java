package de.ceandream.test.sockerstream;

import java.io.*;
import java.net.Socket;

@SuppressWarnings({"deprecation", "ConstantConditions"})
public class Connection extends Thread {

    private Socket socket;
    private BufferedInputStream inputStream;
    private BufferedOutputStream outputStream;

    public Connection(Socket socket) {
        this.socket = socket;
        try {
            this.inputStream = new BufferedInputStream(socket.getInputStream());
            this.outputStream = new BufferedOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {

        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);

        try {
            out.writeUTF("Willkommen!");
            out.writeUTF("Dies ist ein Test vom Server");

            write(b.toByteArray());

            b.close();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }



        while (socket.isConnected()) {
            try {
                DataInputStream in = new DataInputStream(inputStream);

                String line;

                while ((line = in.readUTF()) != null) {
                    System.out.println(line);
                }

                System.out.println("check");

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        System.out.println(socket.getInetAddress().getHostAddress() + " disconnected.");


    }

    public Socket getSocket() {
        return socket;
    }

    public void write (byte[] bytes) {
        try {
            outputStream.write(bytes);
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public BufferedInputStream getInputStream() {
        return inputStream;
    }

    public BufferedOutputStream getOutputStream() {
        return outputStream;
    }
}
